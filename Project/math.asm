;Math Routines for use with MS5837/ROV
    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    #include	<math.inc>
    
MATH code
 mul32
;**********************Multiply 2 32 bit numbers********************************
;At beginning of routine,  product32 contains following:
;[-----upper 4 bytes------][-------lower 4 bytes----------]
;[---------zero-----------][------32 bit multiplier-------]
;		           [bit 0 of multiplier is control mechanism]
;Final Result is held in product32 at end of mul routine
testLsb
    banksel shadowSTATUS
    clrf    shadowSTATUS    ;Clear carry bit of shadow STATUS register
    ; 1) Test Lsb of multiplier (also lsb of product32)
    banksel product32
    btfss   product32, 0
    goto    mulShift	    ;lsb=0 so proceed to shift
addMpcand		    ;lsb=1 so add mpcand32 to left 1/2 of product32
    ;1st Bytes
    banksel mpcand32
    movfw   mpcand32
    addwf   product32+4, f  ;Add byte #0 of mpcand32 to byte #4 of product32
    ;2nd bytes
    movfw   mpcand32+1
    btfsc   STATUS, C
    incfsz  mpcand32+1, w   ;Increment byte #5 of product32 if carry from last addition
    addwf   product32+5, f  ;Add byte #1 of mpcand32 to byte #5 of product32
    ;3rd bytes
    movfw   mpcand32+2
    btfsc   STATUS, C
    incfsz  mpcand32+2, w   ;Increment byte #6 of product32 if carry from last addition
    addwf   product32+6, f  ;Add byte #2 of mpcand32 to byte #6 of product32
    ;4th bytes
    movfw   mpcand32+3
    btfsc   STATUS, C
    incfsz  mpcand32+3, w   ;Increment byte #7 of product32 if carry from last addition
    addwf   product32+7, f  ;Add byte #3 of mpcand32 to byte #7 of product32
    ;Test for carry from last addition
    btfss   STATUS, C
    goto    mulShift
    banksel shadowSTATUS
    bsf	    shadowSTATUS, 0 ;Set carry but of shadow STATUS register so we cam
			    ;righ-shift it at the end of mulShift
    ; 2) Shift all 8 bytes the product32 register right one bit
mulShift
    btfsc   STATUS, C
    ;1st byte
    bcf	    STATUS, C	    ;Clear carry
    banksel product32
    rrf	    product32, f    ;right shift byte #0
    ;2nd byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product32+1, f  ;right shift byte #1
    btfsc   STATUS, C	    ;Carry due to right shift of byte #1?
    bsf	    product32, 7    ;Yes, so shift that bit into byte #0
    ;3rd byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product32+2, f  ;right shift byte #2
    btfsc   STATUS, C	    ;Carry due to right shift of byte #2?
    bsf	    product32+1, 7  ;Yes, so shift that bit into byte #1
    ;4th byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product32+3, f  ;right shift byte #3
    btfsc   STATUS, C	    ;Carry due to right shift of byte #3?
    bsf	    product32+2, 7    ;Yes, so shift that bit into byte #2
    ;5th byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product32+4, f  ;right shift byte #4
    btfsc   STATUS, C	    ;Carry due to right shift of byte #4?
    bsf	    product32+3, 7  ;Yes, so shift that bit into byte #3
    ;6th byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product32+5, f  ;right shift byte #5
    btfsc   STATUS, C	    ;Carry due to right shift of byte #5?
    bsf	    product32+4, 7    ;Yes, so shift that bit into byte #4
    ;7th byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product32+6, f  ;right shift byte #6
    btfsc   STATUS, C	    ;Carry due to right shift of byte #6?
    bsf	    product32+5, 7  ;Yes, so shift that bit into byte #5
    ;8th byte
    bcf	    STATUS, C	    ;Clear carry
    rrf	    product32+7, f  ;right shift byte #7
    btfsc   STATUS, C	    ;Carry due to right shift of byte #7?
    bsf	    product32+6, 7  ;Yes, so shift that bit into byte #6
    ;Check shadowSTATUS carry bit to see if we need to shift it in:
    banksel shadowSTATUS
    btfss   shadowSTATUS, 0
    goto    decLoop
    banksel product32
    bsf	    product32+7, 7  ;Yes so shift it into MSB of product32

decLoop
    ; 3) Decrement loop counter
    banksel loopCount
    decfsz  loopCount, f
    goto    testLsb	    ;reloop 32 times

    retlw	0
;************************End mul32 routine**************************************
    
;**********************Divide 2 32 bit numbers**********************************
div32
; At beginning of routine:
;	remainder = 0 at beginning
;	remainder at end of routine = 33 bits in length
;	loopCount = 32
;	divisor = 32 bits in length
;	Q = dividend (33 bits), but holds quotient at end of routine
divShift
    ;Left-shift Q and remainder together (Q is shifted into remainder)
    bcf		STATUS, C		;Clear carry
    banksel	remainder
    rlf		remainder+4,f		;left shift A (byte 5)
    btfsc	remainder+3, 7		;msb of 4th byte of A = 1?
    bsf		remainder+4, 0		;yes so shift it into 5th byte of A
    
    bcf		STATUS, C		;Clear carry (added this 8/3/2019)???
    rlf		remainder+3,f		;left shift A (byte 4)
    btfsc	remainder+2, 7		;msb of 3rd byte of A = 1?
    bsf		remainder+3, 0		;yes so shift it into 4th byte of A
    
    bcf		STATUS, C		;Clear carry
    rlf		remainder+2,f		;left shift A (byte 3)
    btfsc	remainder+1, 7		;msb of 2nd byte of A = 1?
    bsf		remainder+2, 0		;yes so shift it into 3rd byte of A
    
    bcf		STATUS, C		;Clear carry
    rlf		remainder+1,f		;left shift A (byte 2)
    btfsc	remainder, 7		;msb of 1st byte of A = 1?
    bsf		remainder+1, 0		;yes so shift it into 2nd byte of A
    
    bcf		STATUS, C		;Clear carry
    rlf		remainder,f		;left shift A (byte 1)
    btfsc	Q+4, 7			;msb of 5th byte of Q = 1?
    bsf		remainder, 0		;yes so shift it into 1st byte of A
    
    bcf		STATUS, C		;Clear carry
    rlf		Q+4, f			;left shift Q (byte 5)
    btfsc	Q+3, 7			;msb of 4th byte of Q = 1?
    bsf		Q+4, 0			;yes so shift it into 5th byte of Q
    
    bcf		STATUS, C		;Clear carry
    rlf		Q+3, f			;left shift Q (byte 4)
    btfsc	Q+2, 7			;msb of 3rd byte of Q = 1?
    bsf		Q+3, 0			;yes so shift it into 4th byte of Q
    
    bcf		STATUS, C		;Clear carry
    rlf		Q+2, f			;left shift Q (byte 3)
    btfsc	Q+1, 7			;msb of 2nd byte of Q = 1?
    bsf		Q+2, 0			;yes so shift it into 3rd byte of Q
    
    bcf		STATUS, C		;Clear carry
    rlf		Q+1, f			;left shift Q (byte 2)
    btfsc	Q, 7			;msb of 1st byte of Q = 1?
    bsf		Q+1, 0			;yes so shift it into 2nd byte of Q
    
    bcf		STATUS, C		;Clear carry
    rlf		Q, f			;left shift Q (byte 1)
    
    ; remainder = remainder - divisor
    movfw	divisor
    subwf	remainder, f	;Subtract 1st bytes
    
    movfw	divisor+1
    btfss	STATUS, C	;borrow from subtraction of 1st bytes?
    incfsz	divisor+1, w	;yes so increment 2nd byte to be subtracted (Don't subtract if zero resulted from incrementing)
    subwf	remainder+1, f	;Subtract 2nd bytes
	
    movfw	divisor+2
    btfss	STATUS, C	;borrow from subtraction of 2nd bytes?
    incfsz	divisor+2, w	;yes so increment 3rd byte to be subtracted (Don't subtract if zero resulted from incrementing)
    subwf	remainder+2, f	;Subtract 3rd bytes
	
    movfw	divisor+3
    btfss	STATUS, C	;borrow from subtraction of 3rd bytes?
    incfsz	divisor+3, w	;yes so increment 4th byte to be subtracted (Don't subtract if zero resulted from incrementing)
    subwf	remainder+3, f	;Subtract 4th bytes
    
    ;Extract msb of remainder+4  (not sure if anding is need
    movlw	b'00000001'
    andwf	remainder+4, f
    movlw	.1
    btfss	STATUS, C	;borrow from subtraction of 4th bytes?
    subwf	remainder+4, f
    ;Negative result from subtraction? (Restore if so)
    btfss	STATUS, C	;C=neg number
    goto	resto		;msb of A=1 so restore A
	
    bsf		Q, 0		;no so set lsb of Q
    decfsz	loopCount, f	;Decrement loop counter
    goto	divShift	;Reloop
    goto	divComplete	;Done so exit routine
resto
    banksel	Q
    bcf		Q, 0		;clear lsb of Q
;restore remainder (A = remainder + divisor)
    movfw	divisor
    addwf	remainder, f	;Add 1st bytes
	
    movfw	divisor+1
    btfsc	STATUS, C	;Carry from addition of 1st bytes?
    incfsz	divisor+1, w	;Yes so increment 2nd byte to be added (unless incrementation resulted in a zero value)
    addwf	remainder+1, f	;Add 2nd bytes
	
    movfw	divisor+2
    btfsc	STATUS, C	;Carry from addition of 2nd bytes?
    incfsz	divisor+2, w	;Yes so increment 3rd byte to be added (unless incrementation resulted in a zero value)
    addwf	remainder+2, f	;Add 3rd bytes
	
    movfw	divisor+3
    btfsc	STATUS, C	;Carry from addition of 3rd bytes?
    incfsz	divisor+3, w	;Yes so increment 4th byte to be added (unless incrementation resulted in a zero value)
    addwf	remainder+3, f	;Add 4th bytes
    
    btfss	STATUS, C	;Carry from addition of 4th bytes?
    goto	decrement	;No so proceed to decrement counter
    movlw	.1
    addwf	remainder+4, f	;Yes so add one to 5th byte of A
	
decrement
    decfsz	loopCount, f	;Decrement loop counter
    goto	divShift	;Reloop
	
divComplete
    retlw	0
;****************************End div32 Routine**********************************
    
    
;*****************************Convert Celsius to Farenheit**********************
CtoF
    ;Convert from Celcius to Farenheit (F = (9*degreeC/5) + 32)
	;Place TempC into lower 4 bytes of product32
    movfw	TempC		;TempC is only one byte
    movwf	product32
    movfw	TempC+1
    movwf	product32+1
    movfw	TempC+2
    movwf	product32+2
    movfw	TempC+3
    movwf	product32+3
	;zero out upper 4 bytes of product32
    clrf	product32+4
    clrf	product32+5
    clrf	product32+6
    clrf	product32+7
	;Place d'9' into mpcand32 (4 byte number)
    movlw	.9
    movwf	mpcand32
    clrf	mpcand32+1
    clrf	mpcand32+2
    clrf	mpcand32+3
    movlw	.32
    banksel	loopCount
    movwf	loopCount
    pagesel	mul32	    ;Multiply (9*TempC)
    call	mul32	    ;result of 9 * TempC is in lower 4 bytes of product32
    pagesel$
	;divide product32 (result of 9 * TempC) by 5
	; Zero out remainder
    banksel	remainder
    clrf	remainder
    clrf	remainder+1
    clrf	remainder+2
    clrf	remainder+3
    clrf	remainder+4
	; Place d'5' into divisor
    movlw	.5
    movwf	divisor
    clrf	divisor+1
    clrf	divisor+2
    clrf	divisor+3
	; Place product32 (the result of 9*TempC) into Q (Q is initially the dividend but holds the quotient at 
	; the end of div routine) Q is a 4 byte number
    movfw	product32	
    movwf	Q
    movfw	product32+1
    movwf	Q+1
    movfw	product32+2
    movwf	Q+2
    movfw	product32+3
    movwf	Q+3
    movfw	product32+4
    movwf	Q+4
    
	;loop though 40 times (40 bit division)
    movlw	.40
    banksel	loopCount
    movwf	loopCount
    pagesel	div32
    call	div32	;division result is held in Q 
    pagesel$
	;Is above result negative or positive?
    banksel	negFlag
    btfsc	negFlag, 1
    goto	negC	;Yes because Celsius temp reading was negative, goto negC to handle this
	;Positve so add 32 to LSB of Q
	;CONVERT POSITIVE CELSIUS TO POSITIVE FARENHEIT
    movlw	.32
    banksel	Q
    addwf	Q, f	;Result of Farenheit conversion is now in LSB of Q
    movfw	Q
    movwf	TempF	;Positive Clesius reading converted to positive Farenheit and
					;result is in TempF
    goto	tempDone
	;CELSIUS READING WAS NEGATIVE (WILL DEG F BE NEG OR POS?)
negC
	;Celsius reading was negative so subtract LSB of Q from 32
	;if LSB of Q > than 32, result will be a negative Farenheit reading
    movlw	.32
    banksel	Q
    subwf	Q, w
    btfsc	STATUS, C	;C = 0 if neg result
    goto	posFarenheit
	;CONVERT NEGATIVE CELSIUS TO NEGATIVE FARENHEIT
    bsf  	negFlag, 2	;set flag to indicate a neg farenheit reading
    movfw	Q
    movwf	TempF		;Place LSB of Q into TempF
	;subtract 32 from LSB of Q (which is in TempF
    movlw	.32
    subwf	TempF, f	;Negative Celsius reading converted to a negative Farenheit and 
						;result is in TempF
    goto	tempDone
	;CONVERT NEGATIVE CELSIUS TO POSITIVE FARENHEIT
posFarenheit
    bcf		negFlag, 2  ;clear flag for negative farenheit reading
	;Farenheit conversion will result in a positive number
	;subtract LSB of Q from 32
    movlw	.32
    movwf	TempF	  ;Place 32 into TempF
    movwf	Q		  ;Subtract Q from 32
    subwf	TempF, f  ;Negative Celsius reading converted to positive Farenheit and
					  ;result is in TempF
tempDone
    retlw	0
;*************************End Celsius to Farenheit conversion*******************
 
    END