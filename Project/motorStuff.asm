;Various routines for motor/ESC related tasks
    
    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    
    extern  state
    extern  forwardSpeed
    extern  reverseSpeed
    extern  upDownSpeed
    extern  delayMillis
    extern  transData
    extern  receiveData
    extern  Transmit
    extern  cameraServoPosition
    extern  I2Cstart
    extern  I2CStop
    extern  i2cByteToSend
    extern  waitACK
    extern  I2Csend
    extern  leadScrewPWM
    extern  gripTiltPWM
    extern  gripJawPWM
    extern  lightsPWM
    extern  initCounter
    extern  UartReceiveCtr
    extern  lightsPulse
    
    global  ESCinit
    global  ProcessState
    global  ProcessFwdSpeed
    global  ProcessRevSpeed
    global  ProcessVertSpeed
    global  ProcessCamServo
    global  ProcessLights
    global  ProcessLeadScrew
    global  ProcessGripperTilt
    global  ProcessGripperJaw
    global  ProcessLeak
    global  ProcessReset
    global  LEDpulse
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
	
    #define BANK0  (h'000')
    #define BANK1  (h'080')
    #define BANK2  (h'100')
    #define BANK3  (h'180')
    #define gripperWrite  (b'00010000')	    ;Bus address for i2c gripper controller write (d'16')
    #define LScmd	  (b'00000001')	    ;Command to alerting gripper controller
					    ;that next packet is PWM for lead screw (d'1')
    #define GTcmd	  (b'00000010')	    ;Command to alerting gripper controller
					    ;that next packet is PWM for gripper tilt (d'2')
    #define GJcmd	  (b'00000100')	    ;Command to alerting gripper controller
					    ;that next packet is PWM for gripper claw (d'4')	
    #define screwStop	       (d'94')	    ;PWM stop signal for lead screw	
    #define gripperTiltNeutral (d'88')
    #define LEDread	  (b'00001111')	    ;Bus address for i2c LED controller read
    #define LEDwrite	  (b'00001110')	    ;Bus address for i2c LED controller writes
    #define ROVreset	  (b'10101010')	    ;Command to perform ROV reset (d'170')
    #define noROVreset	  (b'01010101')	    ;Command for no ROV reset (d'85')
    
    

    
;**************Send PWM signals to thrusters************************************
;Once all four UART thruster data packets have been received, send PWM signals
;to thrusters
MOTOR	    code
ProcessState    
    movfw	receiveData
    movwf	state
    movlw	.1
    movwf	UartReceiveCtr	;restart UART reception counter
    ;check directional state of ROV
    movfw	state
    ;use lookup table to get AND values for PORTD
    pagesel	stateLookUp	;Call to stateLookUp appears to cross a page 
    call	stateLookUp	;boundary, hence the use of pagesel
    pagesel$
    banksel	PORTD
    movwf	PORTD	    ;place AND value in PORTD
    ;Respond to surface controller with the state value so it can proceed with
    ;data stream
    movfw	state
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
 
ProcessFwdSpeed
    incf	UartReceiveCtr, f   ;increment UART reception counter
    movfw	receiveData	    ;Place data packet value into forwardSpeed
    movwf	forwardSpeed
    movfw	forwardSpeed
    banksel	CCPR1L
    movwf	CCPR1L
    ;Respond to surface controller with the fwd speed value so it can proceed with
    ;data stream
    movfw	forwardSpeed
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
    
ProcessRevSpeed
    incf	UartReceiveCtr, f   ;increment UART reception counter
    movfw	receiveData	    ;Place data packet value into reverseSpeed
    movwf	reverseSpeed
    movfw	reverseSpeed
    banksel	CCPR2L
    movwf	CCPR2L
    ;Respond to surface controller with the rev speed value so it can proceed with
    ;data stream
    movfw	reverseSpeed
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
    
ProcessVertSpeed
    incf	UartReceiveCtr, f   ;increment UART reception counter
    movfw	receiveData	    ;Place data packet value into upDownSpeed
    movwf	upDownSpeed
    movfw	upDownSpeed
    banksel	CCPR3L
    movwf	CCPR3L
    ;Respond to surface controller with the vertical speed value so it can 
    ;proceed with data stream
    movfw	upDownSpeed
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0 
    
ProcessCamServo
    incf	UartReceiveCtr, f   ;increment UART reception counter
    movfw	receiveData	    ;Place data packet value into camera servo
    movwf	cameraServoPosition ;variable
    movfw	cameraServoPosition
    banksel	CCPR5L
    movwf	CCPR5L
    ;Respond to surface controller with the camera servo position so it can 
    ;proceed with data stream
    movfw	cameraServoPosition
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
  
ProcessLights
    incf	UartReceiveCtr, f   ;increment UART reception counter
    ;Determine if LED PWM value has even changed since last time (in order 
    ;to avoid updating it needlessly)
    movfw	receiveData
    banksel	lightsPWM
    xorwf	lightsPWM, w
    btfsc	STATUS, Z
    goto	LEDupdated	;LED value hasn't changed to don't bother 
				;updating
    movfw	receiveData	
    banksel	lightsPWM	;Value has changed so update the variable
    movwf	lightsPWM
    ;1st make sure the PWM value is within the range of signals acceptable by the
    ;LED. 
    ;movlw	.61		;(Max PWM5DCH value is 60)
    ;banksel	lightsPWM
    ;subwf	lightsPWM, w
    ;btfsc	STATUS, C	;C = 0 if neg result
    ;goto	LEDupdated	;Value is too high so don't update
    
    ;movlw	.34
    ;banksel	lightsPWM
    ;subwf	lightsPWM, w
    ;btfss	STATUS, C	;C = 0 if neg result
    ;goto	LEDupdated	;Value is too low so don't update
    
    ;lightsPWM is a valid value so send it to LED controller
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    
    movlw	LEDwrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
    banksel	lightsPWM
    movfw	lightsPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
    ;Send Stop
    pagesel	I2CStop
    call	I2CStop
    pagesel$
LEDupdated
    ;Respond to surface controller with the Lights value so it can 
    ;proceed with data stream
    banksel	lightsPWM
    movfw	lightsPWM
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
    
ProcessLeadScrew
    incf	UartReceiveCtr, f   ;increment UART reception counter
    ;Determine if lead screw value has even changed since last time (in order 
    ;to avoid updating it needlessly)
    movfw	receiveData
    banksel	leadScrewPWM
    xorwf	leadScrewPWM, w
    btfsc	STATUS, Z
    goto	LeadScrewUpdated    ;Lead screw value hasn't changed to don't bother 
				    ;updating
    movfw	receiveData	    ;Lead screw value has changed so update it
    movwf	leadScrewPWM
    
    ;Send command letting controller know that next packet is a PWM value 
    ;for the lead screw ESC
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    movlw	LScmd
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    
    ;Send lead screw PWM value
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    banksel	leadScrewPWM
    movfw	leadScrewPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    
LeadScrewUpdated
    ;Respond to surface controller with the lead screw value so it can 
    ;proceed with data stream
    banksel	leadScrewPWM
    movfw	leadScrewPWM
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
    
ProcessGripperTilt
    incf	UartReceiveCtr, f   ;increment UART reception counter
    ;Determine if gripper tilt value has even changed since last time (in order 
    ;to avoid updating it needlessly)
    movfw	receiveData
    banksel	gripTiltPWM
    xorwf	gripTiltPWM, w
    btfsc	STATUS, Z
    goto	TiltUpdated    ;Lead screw value hasn't changed to don't bother 
				    ;updating
    movfw	receiveData	    ;Lead screw value has changed so update it
    banksel	gripTiltPWM
    movwf	gripTiltPWM
    
    ;Send command letting controller know that next packet is a PWM value 
    ;for the gripper tilt servo
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    movlw	GTcmd
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    
    ;Send gripper tilt PWM value
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    banksel	gripTiltPWM
    movfw	gripTiltPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
TiltUpdated    
    ;Respond to surface controller with the tilt value so it can 
    ;proceed with data stream
    banksel	gripTiltPWM
    movfw	gripTiltPWM
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
    
ProcessGripperJaw
    incf	UartReceiveCtr, f   ;increment UART reception counter
    ;Determine if gripper jaw value has even changed since last time (in order 
    ;to avoid updating it needlessly)
    movfw	receiveData
    banksel	gripJawPWM
    xorwf	gripJawPWM, w
    btfsc	STATUS, Z
    goto	JawUpdated    ;Lead screw value hasn't changed to don't bother 
				    ;updating
    movfw	receiveData	    ;Lead screw value has changed so update it
    banksel	gripJawPWM
    movwf	gripJawPWM
    
    ;Send command letting controller know that next packet is a PWM value 
    ;for the gripper jaw servo
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    movlw	GJcmd
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    
    ;Send gripper jaw PWM value
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    banksel	gripJawPWM
    movfw	gripJawPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
JawUpdated
    ;Respond to surface controller with the jaw value so it can 
    ;proceed with data stream
    banksel	gripJawPWM
    movfw	gripJawPWM
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    retlw	0
    
ProcessLeak
    incf	UartReceiveCtr, f   ;increment UART reception counter
    movfw	receiveData	    ;Value being sent by controller is irrelevant
    ;Check the status of the leak detector and respond to surface controller with
    ;results (PORTB, 0 = leak detector input, high=leak detected)
    movlw	.1
    banksel	PORTB
    andwf	PORTB, w    ;extract bit#0 of PORTB. (PORTB, 0 is only high in 
    movwf	transData   ;the presence of water.
    incf	transData, f   ;Add "1" to transdata so we won't end up sending
    pagesel	Transmit       ; a zero in a "dry" condition.
    call	Transmit    ;and send it to the surface controller
    pagesel$
    retlw	0
    
ProcessReset
    incf	UartReceiveCtr, f   ;increment UART reception counter
    ;Determine if a reset signal is being sent or not
    movlw	ROVreset
    xorwf	receiveData, w
    btfss	STATUS, Z
    goto	NoReset
YesReset  
    ;Flash LEDS brightly several times to signal reset is taking place
    movlw	.12
    banksel	lightsPulse
    movwf	lightsPulse	;Number of pulse cycles for the LEDs
    pagesel	LEDpulse
    call	LEDpulse
    pagesel$
    
    ;Respond to controller with the reset command so it can proceed.
    movlw	ROVreset
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    movlw	.250
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    RESET
NoReset    
    ;Respond to controller with the no-reset command so it can proceed.
    movlw	noROVreset
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    
    retlw	0

    
    
    
;*******************Lookup Table to get ANDing values for PORTD ****************
stateLookUp
    addwf   PCL, f
    retlw   b'11000011'	    ;0 forward (T1/T2 FWD, T3/T4 REV)
    retlw   b'00111100'	    ;1 reverse (T3/T4 FWD, T1/T2 REV)
    retlw   b'10100101'	    ;2 traverse right (T1/T3 FWD, T2/T4 REV)
    retlw   b'01011010'	    ;3 traverse left (T2/T4 FWD, T1/T3 REV)
    retlw   b'01101001'	    ;4 rotate clockwise (T1/T4 FWD, T2/T3 REV)
    retlw   b'10010110'	    ;5 rotate counter-clockwise (T2/T3 FWD, T1/T4 REV)
    retlw   b'00001111'	    ;6 up/down (T1/T4 FWD, T2/T3 REV)
    retlw   b'00001111'	    ;7 stop
    
;******Initialize ESCs with stoped signal (1500uS) for four seconds*************
ESCinit
    movlw	b'00001111'
    banksel	PORTD
    movwf	PORTD
    movlw	.25
    movwf	initCounter	;16 calls to delayMillis at 250ms each = 4 sec
    movlw	.95		;1500uS pulse width
    banksel	CCPR2L		;ESC #2
    movwf	CCPR2L
    banksel	CCPR1L		;ESC #1
    movwf	CCPR1L
    banksel	CCPR3L
    movwf	CCPR3L
beginInit
    movlw	.250
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    decf	initCounter, f
    movlw	.0
    xorwf	initCounter, w
    btfss	STATUS, Z
    goto	beginInit
    
    retlw	0

;****************Pulse ROV LEDs*************************************************    
LEDpulse
    movlw	.46
    banksel	lightsPWM
    movwf	lightsPWM
    
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    
    movlw	LEDwrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
    banksel	lightsPWM
    movfw	lightsPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
    ;Send Stop
    pagesel	I2CStop
    call	I2CStop
    pagesel$ 
    
    movlw	.250
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    movlw	.34
    banksel	lightsPWM
    movwf	lightsPWM
    
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    
    movlw	LEDwrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
    banksel	lightsPWM
    movfw	lightsPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
    ;Send Stop
    pagesel	I2CStop
    call	I2CStop
    pagesel$ 
    
    movlw	.250
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    banksel	lightsPulse
    decfsz	lightsPulse, f
    goto	LEDpulse
    
    retlw	0    

    END