;Battery monitoring routines
    
    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
    #define battAddrRead       (b'00000101')    ;Bus address for i2c battery (read)
					        ;Address=d'2' but bit #0 is ignored
    #define battAddrWrite      (b'00000100')    ;Bus address for i2c battery (write)
					        ;Address=d'2' but bit #0 is ignored
    #define readCell1	       (b'00000001')	;I2C command to retrieve cell1 value					     
    #define readCell2	       (b'00000010')	;I2C command to retrieve cell2 value
    #define readCell3	       (b'00000011')	;I2C command to retrieve cell3 value
    #define readCell4	       (b'00000100')	;I2C command to retrieve cell4 value
    #define batteryCMD      (d'255')  ;Command from controller to perform battery read

    global  BattRead
    global  Voltage
    
    extern  I2Cstart
    extern  I2CStop
    extern  I2Crestart
    extern  waitMSSP
    extern  sendACK
    extern  sendNACK
    extern  enReceive
    extern  sendI2Cbyte
    extern  I2Csend
    extern  i2cByteToSend   
    extern  i2cByteReceived
    extern  waitACK
    extern  cellCounter
    extern  cell1
    extern  cell2
    extern  cell3
    extern  cell4
    extern  transData
    extern  receiveData
    extern  Transmit
    extern  Receive
    
BATTERY   code
;-------------------------------------------------------------------------------
;*****Read 4 battery cells and send the results to surface controller***********
;-------------------------------------------------------------------------------  
Voltage
   banksel	PIE1
    bcf		PIE1, RCIE	;Disable UART receive interrupts
    call	BattRead	;Get cell 1 value
    call	BattRead	;Get cell 2 value
    call	BattRead	;Get cell 3 value
    call	BattRead	;Get cell 4 value
    ;Respond with battery command
    movlw	batteryCMD
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    
sendCell1
    banksel	cell1
    movfw	cell1
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    ;Wait till controller responds with decimal value "1"
    pagesel	Receive
    call	Receive
    pagesel$
    movlw	.1
    xorwf	receiveData, w
    btfss	STATUS, Z	;Did we reveive "1" yet?
    goto	sendCell1
    
sendCell2
    banksel	cell2
    movfw	cell2
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    ;Wait till controller responds with decimal value "2"
    pagesel	Receive
    call	Receive
    pagesel$
    movlw	.2
    xorwf	receiveData, w
    btfss	STATUS, Z	;Did we reveive "2" yet?
    goto	sendCell2   
    
sendCell3
    banksel	cell3
    movfw	cell3
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    ;Wait till controller responds with decimal value "3"
    pagesel	Receive
    call	Receive
    pagesel$
    movlw	.3
    xorwf	receiveData, w
    btfss	STATUS, Z	;Did we reveive "3" yet?
    goto	sendCell3   
    
sendCell4
    banksel	cell4
    movfw	cell4
    banksel	transData
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    ;Wait till controller responds with decimal value "4"
    pagesel	Receive
    call	Receive
    pagesel$
    movlw	.4
    xorwf	receiveData, w
    btfss	STATUS, Z	;Did we reveive "4" yet?
    goto	sendCell4 
    banksel	PIE1
    bsf		PIE1, RCIE	;Enable UART receive interrupts
    
    retlw	0
   
   
BattRead
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	battAddrWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
    ;Determine which cell we need to read
    movlw	.0		;Cell #1?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell1Command
    
    movlw	.1		;Cell #2?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell2Command
    
    movlw	.2		;Cell #3?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell3Command
    
    movlw	.3		;Cell #4?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell4Command
    
;Load the appropriate command
Cell1Command
    movlw	readCell1
    goto	SendIt
Cell2Command
    movlw	readCell2
    goto	SendIt  
Cell3Command
    movlw	readCell3
    goto	SendIt
Cell4Command
    movlw	readCell4
    goto	SendIt
    
;Send the command
SendIt
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$    
;Send Stop
    pagesel     I2CStop
    call	I2CStop
    pagesel$
    
;Read cell data
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	battAddrRead
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$    
    
;Receive byte:
    pagesel	enReceive
    call	enReceive
    pagesel$
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$    
    
;Determine which cell we are receiving
    movlw	.0		;Cell #1?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell1
    
    movlw	.1		;Cell #2?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell2
    
    movlw	.2		;Cell #3?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell3
    
    movlw	.3		;Cell #4?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell4    
    
;Store the cell reading
StoreCell1    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell1
    movwf	cell1
    banksel	cellCounter
    incf	cellCounter,f	;Increment the counter.
    goto	DoneStoring
StoreCell2    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell2
    movwf	cell2
    banksel	cellCounter
    incf	cellCounter,f	;Increment the counter.
    goto	DoneStoring   
StoreCell3    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell3
    movwf	cell3
    banksel	cellCounter
    incf	cellCounter,f	;Increment the counter.
    goto	DoneStoring    
StoreCell4    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell4
    movwf	cell4
    banksel	cellCounter
    clrf	cellCounter	;Reset counter
    goto	DoneStoring     
    
DoneStoring    
    ;Send ACK
    pagesel	sendNACK
    call	sendNACK
    pagesel$
    ;Send Stop
    pagesel     I2CStop
    call	I2CStop
    pagesel$    
    
   retlw    0
   
   
    
    
    END