;Pressure monitoring routines for MS5837 module
    
    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
    
    global	CalculatePressure
    
    extern  eightByteNum1
    extern  eightByteNum2
    extern  Q
    extern  div32
    extern  negFlag
    extern  loopCount
    extern  product32
    extern  remainder
    extern  divisor
    extern  mul32
    extern  mpcand32
    extern  deeT
    extern  OFF
    extern  TCO
    
    
;-------------------------------------------------------------------------------
;************************************Pressure Calculations**********************
;-------------------------------------------------------------------------------
PRESSURE	code    
CalculatePressure
    banksel	eightByteNum1
    clrf	eightByteNum1
    clrf	eightByteNum1+1
    clrf	eightByteNum1+2
    clrf	eightByteNum1+3
    clrf	eightByteNum1+4
    clrf	eightByteNum1+5
    clrf	eightByteNum1+6
    clrf	eightByteNum1+7
    ;Calculate Pressure Value:\
    ;Multiply OFF (2 bytes) by 2^16 (3 bytes)
    banksel	OFF
    movfw	OFF
    movwf	product32
    movfw	OFF+1
    movwf	product32+1
    clrf	product32+2
    clrf	product32+3
    clrf	product32+4
    clrf	product32+5
    clrf	product32+6
    clrf	product32+7	;Place OFF into lower 4 bytes of product32
    
    banksel	mpcand32
    clrf	mpcand32
    clrf	mpcand32+1
    movlw	.1
    movwf	mpcand32+2
    clrf	mpcand32+3	;Place 2^15 into mpcand32
    
    movlw	.32
    movwf	loopCount
    pagesel	mul32
    call	mul32	    ;Multiply OFF by 2^16 (Result is in product 32)
    pagesel$
    
    ;Place result of above multiplication into eightByteNum1
    ;(Remember that 8byteNum1 is in different bank)
    movfw	product32
    banksel	eightByteNum1
    movwf	eightByteNum1	;byte 1
    banksel	product32
    movfw	product32+1
    banksel	eightByteNum1
    movwf	eightByteNum1+1	;byte2
    banksel	product32
    movfw	product32+2
    banksel	eightByteNum1
    movwf	eightByteNum1+2	;byte3
    banksel	product32
    movfw	product32+3
    banksel	eightByteNum1
    movwf	eightByteNum1+3	;byte4	(max of 4 bytes from above mul32 call)
    
    ;Multiply TCO (2 bytes) by dT (4 bytes)\
    banksel	deeT
    movfw	deeT		
    movwf	product32
    movfw	deeT+1
    movwf	product32+1
    movfw	deeT+2
    movwf	product32+2
    movfw	deeT+3
    movwf	product32+3	;Place deeT in lower 4 bytes of product32
    
    clrf	product32+4
    clrf	product32+5
    clrf	product32+6
    clrf	product32+7	;Zero out upper 4 bytes of product32
    
    banksel	TCO
    movfw	TCO
    movwf	mpcand32
    movfw	TCO+1
    movwf	mpcand32+1
    clrf	mpcand32+2
    clrf	mpcand32+3	;Place TCO into mpcand32
    
    movlw	.32
    banksel	loopCount
    movwf	loopCount
    pagesel	mul32	    ;Multiply TCO by dT (Result is in product 32)
    call	mul32	    
    pagesel$
    
    ;Divide multiplication result from above by 2^7
    ;Zero out remainder
    banksel	remainder
    clrf	remainder
    clrf	remainder+1
    clrf	remainder+2
    clrf	remainder+3
    clrf	remainder+4
    
    ;Place 2^7 (128) into divisor/M (4 bytes)
    movlw	.128
    movwf	divisor
    clrf	divisor+1
    clrf	divisor+2
    clrf	divisor+3
    
    ;Place product32 into Q (Q is initially the dividend but holds
    ;the quotient at the end of div routine
    banksel	product32
    movfw	product32	
    movwf	Q
    movfw	product32+1
    movwf	Q+1
    movfw	product32+2
    movwf	Q+2
    movfw	product32+3
    movwf	Q+3
    movfw	product32+4
    movwf	Q+4
    
    ;loop though 40 times (40 bit division)
    movlw	.40
    banksel	loopCount
    movwf	loopCount
    pagesel	div32
    call	div32	;division result is held in Q
    pagesel$
    
    ;Is deeT a negative number?
    banksel	negFlag	
    btfsc	negFlag, 0  ;Bit #0 of negFlag is set if deeT is negative
    goto	Sub1
    ;deeT is positive, so add Q (5 bytes) to eightByteNum1 (8 bytes but only lower 4 should have anything)
    banksel	Q
    movfw	Q
    banksel	eightByteNum1
    addwf	eightByteNum1, f    ;Add 1st bytes
    
    banksel	Q
    movfw	Q+1
    btfss	STATUS, C	    ;Carry from addition of last byte?
    goto	$+5		    ;No so proceed to add next byte.
    movlw	.1		    ;Yes so add 1 extra.
    addwf	Q+1, w
    btfsc	STATUS, C	    ;Did this overflow the register?
    goto	$+3		    ;Yes so jump ahead.
    banksel	eightByteNum1
    addwf	eightByteNum1+1, f   ;Add 2nd bytes
    
    banksel	Q
    movfw	Q+2
    btfss	STATUS, C	    ;Carry from addition of last byte?
    goto	$+5		    ;No so proceed to add next byte.
    movlw	.1		    ;Yes so add 1 extra.
    addwf	Q+2, w
    btfsc	STATUS, C	    ;Did this overflow the register?
    goto	$+3		    ;Yes so jump ahead.
    banksel	eightByteNum1
    addwf	eightByteNum1+2, f   ;Add 3rd bytes
    
    banksel	Q
    movfw	Q+3
    btfss	STATUS, C	    ;Carry from addition of last byte?
    goto	$+5		    ;No so proceed to add this byte.
    movlw	.1		    ;Yes so add 1 extra.
    addwf	Q+3, w
    btfsc	STATUS, C	    ;Did this overflow the register?
    goto	$+3		    ;Yes so jump ahead.
    banksel	eightByteNum1
    addwf	eightByteNum1+3, f   ;Add 4th bytes
    
    banksel	Q
    movfw	Q+4
    btfss	STATUS, C	    ;Carry from addition of last byte?
    goto	$+5		    ;No so proceed to add next byte.
    movlw	.1		    ;Yes so add 1 extra.
    addwf	Q+4, w
    btfsc	STATUS, C	    ;Did this overflow the register?
    goto	$+3		    ;Yes so jump ahead.
    banksel	eightByteNum1
    addwf	eightByteNum1+4, f   ;Add 5th bytes
    
    btfsc	STATUS, C	    ;Carry from addition of last bytes?
    incf	eightByteNum1+5, f
    
;deeT is negative, so subtract Q (5 bytes) from eightByteNum1
Sub1
    banksel	Q
    movfw	Q
    banksel	eightByteNum1
    subwf	eightByteNum1, f    ;Subtract 1st bytes.
    
    banksel	Q
    movfw	Q+1
    btfsc	STATUS, C	    ;borrow from subtraction of 1st bytes? (C = 0 if neg result)
    goto	$+4		    ;No so proceed to subtract bytes
    incf	Q+1, w		    ;yes so increment 2nd byte to be subtracted 
    btfsc	STATUS, C	    ;(Don't subtract if zero resulted from incrementing)
    goto	$+2
    banksel	eightByteNum1				    
    subwf	eightByteNum1+1, f
    
    movlw	.0
    
    retlw	0    
    
    
    
    
    
    
    
    END


