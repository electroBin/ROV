;config file for main.asm
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
    extern  Transmit
    extern  Receive
    extern  ESCinit
    extern  peripheralInit
    extern  slaveReset
    extern  BattRead
    extern  PWMinit
    extern  slaveReset
    extern  Voltage	    ;Routine to perform a battery voltage reading
    extern  Degrees	    ;Routine to perform a temperature reading
    extern  ProcessState
    extern  ProcessFwdSpeed
    extern  ProcessRevSpeed
    extern  ProcessVertSpeed
    extern  ProcessCamServo
    extern  ProcessLights
    extern  ProcessLeadScrew
    extern  ProcessGripperTilt
    extern  ProcessGripperJaw
    extern  ProcessLeak
    extern  ProcessReset
    
    ;Leak
    ;Delay routine
    global  delayMillis
    global  forwardSpeed
    global  reverseSpeed
    global  upDownSpeed
    global  transData
    global  receiveData
    global  state
    global  UartReceiveCtr
    global  sensorCtr
    ;math.asm
    global  product32
    global  mpcand32
    global  loopCount
    global  negFlag
    global  deeT
    global  remainder
    global  divisor
    global  Q
    global  TempC
    global  TempF
    global  eightByteNum1
    global  eightByteNum2
    ;global  negFaren
    global  shadowSTATUS
    ;ms5837.asm
    global  SENS
    global  OFF
    global  TCS
    global  TCO
    global  Tref
    global  TEMPSENS
    global  D1
    global  D2
    ;serial.asm
    extern  I2Cstart
    extern  I2CStop
    extern  I2Csend
    extern  waitACK
    global  i2cByteToSend  
    global  i2cByteReceived
    global  coeffCPY	    
    global  adcCPY	
    ;camera servo
    global  cameraServoPosition
    ;ESCx/Motors
    global  initCounter
    ;Battery
    global  cellCounter
    global  cell1
    global  cell2
    global  cell3
    global  cell4
    ;Lights
    global  lightsPWM
    global  lightsPulse
    extern  LEDpulse
    ;Gripper arm
    global  leadScrewPWM   
    global  gripTiltPWM	    
    global  gripJawPWM	    
    
	
    #define BANK0  (h'000')
    #define BANK1  (h'080')
    #define BANK2  (h'100')
    #define BANK3  (h'180')
    
    #define batteryCMD      (d'255')  ;Command from controller to perform battery read	
    #define tempCMD         (d'254')  ;Command from controller to perform temperature read
    #define leakCMD	    (d'253')  ;Command from controller to perfrom leak test
				      ;(done as part of normal datastream communication)
    
    ; CONFIG1
; __config 0x9C2
    __CONFIG _CONFIG1, _FOSC_HS & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF
; CONFIG2
; __config 0xDDFF
    __CONFIG _CONFIG2, _WRT_OFF & _VCAPEN_OFF & _PLLEN_ON & _STVREN_OFF & _BORV_LO & _LVP_OFF
    
;************Variables accessible in all banks (No more than 16 bytes):*********
    MULTIBANK	        UDATA_SHR
    transData		RES 1	;Data to be transmitted via UART
    receiveData		RES 1	;Data received via UART
    state		RES 1	;Direction "state" of ROV
    forwardSpeed	RES 1	;value to be placed in CCPR1L
    reverseSpeed	RES 1	;value to be placed in CCPR2L
    upDownSpeed		RES 1	;value to be placed in CCPR3L
    UartReceiveCtr	RES 1	;counter for number of UART receptions
    userMillis		RES 1
    w_copy		RES 1	;variable used for context saving (work reg)
    status_copy		RES 1	;variable used for context saving (status reg)
    pclath_copy		RES 1	;variable used for context saving (pclath copy)
    sensorCtr		RES 1	;Incremented on every UART receive interrupt.
				;At 5 sec, sensors are read
    ;Camera Servo
    cameraServoPosition	RES 1   ;Holds value to place in CCPR5L for camera servo
    initCounter		RES 1	;counter for initializing ESCs
	    
	    
	    
;******Banked variables #1  (No more than 80 bytes per non-shared bank)*********
    BANKED1	    UDATA   ;(0x20-0x6F)
    ;math.asm
    product32	    RES	8   ;64 bit 
    mpcand32	    RES	4   ;32 bit multiplicand for multiplication routine.
    loopCount	    RES	1   ;counter mul/div routines
    negFlag	    RES	1   ;bit 0 of this is set is operation results in neg number
			    ;b'00000000'	   
			    ;  -------1	    sign of deeT
			    ;  ------10	    sign of temp (in celsius)
			    ;  -----1--	    sign of temp (in Farenheit)
    deeT	    RES	4   ;32 bit signed int
    ;32 bit div variables
    remainder	    RES	5   ;40 bit number (Remainder for div32 routine)
    divisor	    RES	4   ;divisor
    Q		    RES	5   ;dividend/quotient
    TempC	    RES	5   ;Final value for temperature (Celsius) reading (signed value)
			    ;LSB of TempC holds value to be displayed
    TempF	    RES	1   ;Final value for temperature (Farenheit) (signed value)
    ;negFaren	    RES	1   ;Flag used to indicate a negative temp in Farenheit
			    ;if bit 0 = 1 then negative
    shadowSTATUS     RES	1   ;Copy of STATUS register used in mul32 routine
    ;For ms5837.asm
    SENS	    RES 2	;Pressure Secnsitivity (2 bytes) from PROM
    OFF		    RES 2	;Pressure Offset (2 bytes) from PROM
    TCS		    RES 2	;Temp coeff of pressure sensitivity (2 bytes) from PROM
    TCO		    RES 2	;Temp coeff of pressure offset (2 bytes) from PROM
    Tref	    RES 2	;Reference temperature (2 bytes) from PROM
    TEMPSENS	    RES 2	;Temp coeff of temperature (2 bytes) from PROM
    D1		    RES 3	;Pressure value from ADC read of slave (3 bytes)
    D2		    RES 3	;Temperature value from ADC read of slave (3 bytes)
    ;For serial.asm
    i2cByteToSend   RES	1
    i2cByteReceived RES	1
    coeffCPY	    RES	2   ;shadow register for copying PROM coefficients
    adcCPY	    RES	3   ;shadow register for copying temp/press ADC values
    
;*******Banked variables #2  (No more than 80 bytes per non-shared bank)********
    BANKED2	    UDATA   ;(0xA0-0xEF)
    eightByteNum1   RES	8   ;8-byte variable for storage
    eightByteNum2   RES	8   ;8-byte variable for storage
    ;Battery
    cellCounter	    RES	1	;Counter to keep track of which cell is being
				;read	    
    cell1	    RES	1
    cell2	    RES	1
    cell3	    RES	1
    cell4	    RES	1	
    lightsPWM	    RES	1	;PWM value for LED lights	    
    leadScrewPWM    RES	1	;PWM value to be sent to lead screw motor
    gripTiltPWM	    RES	1	;PWM value to be sent to gripper tilt servo
    gripJawPWM	    RES	1	;PWM value to be sent to gripper claw servo
    lightsPulse	    RES	1	;Counter to keep track of the number of LED
				;pulses
    
    	    
   