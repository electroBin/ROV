;config file for math.asm
    
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
    
    extern  product32
    extern  mpcand32
    extern  loopCount
    extern  negFlag
    extern  deeT
    extern  remainder
    extern  divisor
    extern  Q
    extern  TempC
    extern  TempF
    ;extern  negFaren
    extern  shadowSTATUS
    
    global  CtoF
    global  mul32
    global  div32
    
   
    
    