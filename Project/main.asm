;Receive PWM values from topside via UART, decode packets and send PWM value to 
;appropriate thruster ESCs (Main file for ROV)

    list	p=16f1937	   ;list directive to define processor
    #include	<p16f1937.inc>	   ;processor specific variable definitions
    #include	<main.inc>	   ;include file for main.asm
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -207    ;no label after column one warning
    
;**********************************************************************
.reset code	0x000	
    pagesel	start	    ;processor reset vector
    goto	start	    ;go to beginning of program
    nop
INT_VECTOR:
    ORG		0x004		; interrupt vector location
INTERRUPT:
    movwf       w_copy           ;save off current W register contents
    movf	STATUS,w         ;move status register into W register
    movwf	status_copy      ;save off contents of STATUS register
    movf	PCLATH,W
    movwf       pclath_copy
  
;*********************BEGIN UART INTERRUPT**************************************
UartReceive
    pagesel	Receive
    call	Receive
    pagesel$
    ;Determine if UART packet is a command to perform a battery reading, a temperature
    ;reading or is part of the normal data stream
    movlw	batteryCMD
    xorwf	receiveData, w
    btfsc	STATUS, Z
    goto	BAT
    movlw	tempCMD
    xorwf	receiveData, w
    btfsc	STATUS, Z
    goto	DEG
    goto	DataStream
    
BAT
    pagesel	Voltage
    call	Voltage
    pagesel$
    goto	isrEnd
DEG    
    pagesel	Degrees
    call	Degrees
    pagesel$
    goto	isrEnd
;-------------------------------------------------------------------------------
;***************UART packet is part of the data stream**************************
;-------------------------------------------------------------------------------    
DataStream    
    banksel	PIE1
    bcf		PIE1, RCIE	;Disable UART receive interrupts
    ;1)Get "state" of ROV direction signal
    ;Check if UART packet contains a valid value for "state" (1-7)
State 
    movlw	.8		;max number for "state"=7
    subwf	receiveData, w	;subtract 8 from value in UART packet
    btfsc	STATUS, C	;(C=0 is neg number) (valid result=neg #)
    goto	checkForwardSpeed
    pagesel	ProcessState
    call	ProcessState	;Packet contains state data so process it
    pagesel$
    goto	isrEnd
   
    ;If UartReceiveCtr is "1" then get forward speed
checkForwardSpeed
    movlw	.1
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	checkReverseSpeed   ;Not "1" so proceed
    pagesel	ProcessFwdSpeed
    call	ProcessFwdSpeed	    ;Packet contains fwd speed data so 
    pagesel$			    ;process it
    goto	isrEnd
    ;If UartReceiveCtr is "2" then get reverse speed
checkReverseSpeed
    movlw	.2
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	checkUpDownSpeed   ;Not "2" so proceed
    pagesel	ProcessRevSpeed
    call	ProcessRevSpeed	    ;Packet contains rev speed data so 
    pagesel$			    ;process it
    goto	isrEnd
    ;If UartReceiveCtr is "3" then get up/down speed
checkUpDownSpeed
    movlw	.3
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	checkCameraServo   ;Not "3" so proceed
    pagesel	ProcessVertSpeed
    call	ProcessVertSpeed    ;Packet contains vertical speed data so 
    pagesel$			    ;process it
    goto	isrEnd
    ;If UartReceiveCtr is "4" then get camera servo position
checkCameraServo
    movlw	.4
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	checkLights	     ;Not "4" so proceed
    pagesel	ProcessCamServo
    call	ProcessCamServo    ;Packet contains camera tilt data so 
    pagesel$
    goto	isrEnd
    ;If UartReceiveCtr is "5" then get LED lights value
checkLights
    movlw	.5
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	checkLeadScrew	     ;Not "5" so proceed
    pagesel	ProcessLights
    call	ProcessLights    ;Packet contains camera tilt data so 
    pagesel$
    goto	isrEnd
    ;If UartReceiveCtr is "6" then get lead screw value
checkLeadScrew
    movlw	.6
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	checkGripperTilt	     ;Not "6" so proceed
    pagesel	ProcessLeadScrew
    call	ProcessLeadScrew    ;Packet contains camera tilt data so 
    pagesel$
    goto	isrEnd
    ;If UartReceiveCtr is "7" then get gripper tilt value
checkGripperTilt
    movlw	.7
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	checkGripperJaws
    pagesel	ProcessGripperTilt
    call	ProcessGripperTilt
    pagesel$
    goto	isrEnd
    ;If UartReceiveCtr is "8" then get gripper jaws value
checkGripperJaws   
    movlw	.8
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	CheckLeak
    pagesel	ProcessGripperJaw
    call	ProcessGripperJaw
    pagesel$
    goto	isrEnd
    ;If UartReceiveCtr is "9" then check the leak detector
CheckLeak  
    movlw	.9
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	CheckReset
    pagesel	ProcessLeak
    call	ProcessLeak
    pagesel$
    goto	isrEnd
CheckReset
    movlw	.10
    xorwf	UartReceiveCtr, w
    btfss	STATUS, Z
    goto	isrEnd
    pagesel	ProcessReset
    call	ProcessReset
    pagesel$
    goto	isrEnd

;***************************END UART RECEIVE INTERRUPT**************************
;restore pre-ISR values to registers
isrEnd
    movf	pclath_copy,W
    movwf	PCLATH
    movf	status_copy,w   ;retrieve copy of STATUS register
    movwf	STATUS          ;restore pre-isr STATUS register contents
    swapf	w_copy,f
    swapf	w_copy,w        ;restore pre-isr W register contents
    banksel	PIE1
    bsf		PIE1, RCIE	;enable UART receive interrupts
    
    retfie                      ;return from interrupt
    
    
MAIN    code    
;Variable length delay routine (1-255 milliseconds)    
delayMillis
    movwf	userMillis	;user defined number of milliseconds
startDly
    banksel	TMR0
    clrf	TMR0
waitTmr0
    movlw	.31		;31 * 32uS = 1mS
    banksel	TMR0
    subwf	TMR0, w
    btfss	STATUS, C	;C=0 if neg result 
    goto	waitTmr0
    decfsz	userMillis, f	;reached user defined milliseconds yet?
    goto	startDly
    
    retlw	0
   	
start:
    pagesel	peripheralInit
    call	peripheralInit	    ;initialize peripherals
    pagesel$
    
    movlw	.2
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    ;Initialize PWM module
    pagesel	PWMinit
    call	PWMinit
    pagesel$
    
    movlw	.2
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    ;Initialize ESCs
    pagesel	ESCinit
    call	ESCinit
    pagesel$
    
    pagesel	slaveReset
    call	slaveReset
    pagesel$
    
    banksel	cellCounter
    clrf	cellCounter
    
    ;Wait a few seconds until gripper arm controller is initialized. Do this by
    ;pulsing the LEDs for a few seconds
    movlw	.12
    banksel	lightsPulse
    movwf	lightsPulse	;Number of pulse cycles for the LEDs
    pagesel	LEDpulse
    call	LEDpulse
    pagesel$
    
    clrf	receiveData
    ;Signal to surface controller that ROV is ready
AllGood
    movlw	.1
    call	delayMillis
    
    movlw	.245
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    ;Wait until surface controller sends the 1st data stream.
    banksel	PIR1
    btfss	PIR1, RCIF	;Is RX buffer full? (1=full, 0=notfull)
    goto	AllGood
    
    pagesel	Receive
    call	Receive
    pagesel$
    movlw	.245
    xorwf	receiveData, w
    btfsc	STATUS, Z
    goto	AllGood
    
    movlw	.5
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    ;Enable UART receive interrupts now that everything is initialized
    movlw	b'00100000'
		 ;--1-----	;Enable USART receive interrupt (RCIE=1)
    banksel	PIE1
    movwf	PIE1
 
mainLoop
    
    goto	mainLoop
    
    END                       
































